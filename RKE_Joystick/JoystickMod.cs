using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Reflection;


namespace RKE_Joystick
{
	[KSPAddon(KSPAddon.Startup.Flight, false)]
	public class JoystickMod : MonoBehaviour
	{
		public static string VERSION = "unknown";

		// seems to ensure we are not shown when GUI is hidden
		private const int RENDER_MANAGER_QUEUE_SPOT = 5;

		private JoystickPane jpane;
        private RKEState ourstate;
		private ThrottleControl throttleControl = new ThrottleControl();

        private Rect kbSliderRect;

        private const int _mainWindowID = 1928373;
		private Vector2 windowSize = new Vector2(10, 10);

		private Vessel currentVessel;

        private MethodInfo dynToggleJetpack;

        private EVAControlState estate = new EVAControlState();

		public JoystickMod ()
		{
			try {
                VERSION = typeof(JoystickMod).Assembly.GetName().Version.ToString();
                Debug.Log("RKE_Joystick version " + VERSION);
                MyResources.loadResources();

                jpane = new JoystickPane();

                dynToggleJetpack = typeof(KerbalEVA).GetMethod("ToggleJetpack", BindingFlags.NonPublic | BindingFlags.Instance);
                if (((System.Object)dynToggleJetpack) == null)
                {
                    Debug.LogWarning("RKE: Could not find ToggleJetpack on KerbalEVA");
                }
            } catch (Exception ex) {
                LogEventError("Constructor", ex);
            }
		}

		public void Awake()
		{
			try {
                RenderingManager.AddToPostDrawQueue(RENDER_MANAGER_QUEUE_SPOT, DrawGUI);
            } catch (Exception ex) {
                LogEventError("Awake", ex);
            }
		}

        public void Start() 
        {
            try {
                ourstate = gameObject.AddComponent<RKEState>();
                gameObject.AddComponent<RKEApplicationButton>();
                jpane.init(ourstate);
            } catch (Exception ex) {
                LogEventError("Start", ex);
            }

        }

		public void OnDestroy() {
			try {
                RenderingManager.RemoveFromPostDrawQueue(RENDER_MANAGER_QUEUE_SPOT, DrawGUI);
            } catch (Exception ex) {
                LogEventError("OnDestroy", ex);
            }

		}

		void FixedUpdate()
        {
            try
            {
                if (currentVessel != FlightGlobals.ActiveVessel)
                {
                    if (currentVessel != null)
                    {
                        currentVessel.OnFlyByWire -= flyit;
                    }
                    currentVessel = FlightGlobals.ActiveVessel;
                    currentVessel.OnFlyByWire += flyit;
                
                    ourstate.wantResize = true;
        
                    if (currentVessel.isEVA)
                    {
                        jpane.setStickMode(JoystickPaneMode.EVA_PACK);
                    }
                    else
                    {
                        if (jpane.getStickMode() == JoystickPaneMode.EVA_PACK)
                        {
                            jpane.setStickMode(JoystickPaneMode.PITCHYAW);
                        }
                    }
                
                    if (currentVessel.Landed && string.Equals("Runway", currentVessel.landedAt, StringComparison.OrdinalIgnoreCase))
                    {
                        jpane.setStickMode(JoystickPaneMode.PITCHROLL);
                    }
                
                }
            }
            catch (Exception ex)
            {
                LogEventError("FixedUpdate", ex);
            }
        }

		void Update() {
			try {
                if (ourstate.getBindingKeyDown())
                {
                    ourstate.windowVisible = !ourstate.windowVisible;
                }
                jpane.Update();
            } catch (Exception ex) {
                LogEventError("Update", ex);
            }
		}
		void flyit(FlightCtrlState state)
		{
            try
            {
                jpane.flyit(currentVessel, state, estate);
                throttleControl.flyit(state);
            }
            catch (Exception ex)
            {
                LogEventError("flyit", ex);
            }
		}


		private void DrawGUI () {
            try
            {
                if (PauseMenu.isOpen || FlightDriver.Pause)
                {
                    // Alternativelu, we could have registared for GameEvents.onHideUI/onShowUI
                    return; // don't show anything (otherwise we are still clickable)
                }
                
                GUI.skin = HighLogic.Skin;
                
                if (ourstate.wantResize)
                {
                    windowSize = new Vector2();
                    jpane.resetSize();
                    ourstate.wantResize = false;
                }

                if (ourstate.windowVisible)
                {
                    if (ourstate.fullscreen)
                    {
                        FlightCtrlState state = currentVessel.ctrlState;
                        float w = jpane.minsize.x;
                        float h = jpane.minsize.y;
                        Rect r = new Rect();
                        r.x = (Screen.width - w) / 2;
                        r.y = (Screen.height - h) / 2;
                        r.width = w;
                        r.height = h;

                        GUILayout.BeginArea(r);
                        jpane.onGUI(state, estate);
                        GUILayout.EndArea();

                    }
                    else {
                        var wStyle = new GUIStyle(GUI.skin.window);
                        wStyle.padding.top = 0;
                        wStyle.padding.bottom = 0;
                        //                wStyle.contentOffset += new Vector2(0, 10);
                        Rect windowPosition = new Rect();
                        windowPosition.position = ourstate.windowPosition;
                        windowPosition.size = windowSize;
                        windowPosition = GUILayout.Window(_mainWindowID, windowPosition, myOnJoystickWindow, "", wStyle);
                        windowPosition = constrainToScreen(windowPosition);
                        ourstate.windowPosition = windowPosition.position;
                        windowSize = windowPosition.size;
                    }
                }
                
                GUI.skin = null;
            }
            catch (Exception ex)
            {
                LogEventError("DrawGUI", ex);
            }

		}

        private bool currentJetpackDeployed;
		private void myOnJoystickWindow (int id)
		{
            try {
                if (currentVessel == null) {
                    return;
                }

                FlightCtrlState state = currentVessel.ctrlState;
                var kerbal = currentVessel.GetComponent<KerbalEVA>();
                var eva = kerbal != null;
                var jetpack = kerbal && kerbal.JetpackDeployed;
                var walking = kerbal && !jetpack;

                if (currentJetpackDeployed != jetpack)
                {
                    ourstate.wantResize = true;
                    currentJetpackDeployed = jetpack;
                }

                GUILayout.BeginVertical(); {
                    GUILayout.BeginVertical(); {
                        if (walking) {
                            DrawWalkingMode(kerbal);
                        }
                        else {
                            drawToolbars(eva);
                            GUILayout.BeginHorizontal(); {
                                if (!eva) {
                                    throttleControl.drawThrottle();
                                }
                                jpane.onGUI(state, estate);
                            }
                            GUILayout.EndHorizontal();
                        }
                    }
                    GUILayout.EndVertical();
                    drawStatusBar();
                }
                GUILayout.EndVertical();
                

                if (!jpane.inside(Event.current.mousePosition)) {
                    GUI.DragWindow();
                }
            } catch (Exception ex) {
                LogEventError("myOnJoystickWindow", ex);
            }

		}

        public static void LogEventError(string msg, Exception ex)
        {
            Debug.LogError("RKE-" + msg + " " + ex.ToString());
            Debug.LogError(ex.StackTrace);
        }

        bool IsEVA()
        {
            return jpane.IsEVA();
        }

        String[][] GROUND_MSG = new String[][]
            { 
                new String[] { "Forward", GameSettings.EVA_forward.name },
                new String[] { "Back", GameSettings.EVA_back.name },
                new String[] { "Left", GameSettings.EVA_left.name },
                new String[] { "Right", GameSettings.EVA_right.name },
                new String[] { "Run", GameSettings.EVA_Run.name + " (hold)" },
                new String[] { "Jump", GameSettings.EVA_Jump.name },
                new String[] { "Jetpack", GameSettings.EVA_TogglePack.name },
            };
        String[][] FLOAT_MSG = new String[][]
            { 
                new String[] { "Jetpack", GameSettings.EVA_TogglePack.name },
            };
        String[][] LADDER_MSG = new String[][]
            { 
                new String[] { "Up", GameSettings.EVA_forward.name },
                new String[] { "Down", GameSettings.EVA_back.name },
                new String[] { "Let go", GameSettings.EVA_Jump.name },
            };

        void DrawWalkingMode(KerbalEVA kerbal)
        {
            bool nearGround = Math.Abs(currentVessel.altitude - currentVessel.terrainAltitude) < 10;
            String msg;
            String[][] texts;
            if (kerbal.OnALadder)
            {
                msg = "Climbing";
                texts = LADDER_MSG;
            }
            else if (currentVessel.Splashed)
            {
                msg = "Splashing";
                texts = GROUND_MSG;
            }
            else if (nearGround)
            {
                msg = "Landed";
                texts = GROUND_MSG;
            }
            else
            {
                msg = "Floating";
                texts = FLOAT_MSG;
            }

            GUILayout.BeginHorizontal();
            {
                GUIStyle tStyle = new GUIStyle(GUI.skin.label);
                tStyle.alignment = TextAnchor.MiddleCenter;
                GUILayout.Label(new GUIContent(MyResources.joystick_icon), tStyle);
                GUILayout.Label("EVA " + msg, GUILayout.MinWidth(200));
            }
            GUILayout.EndHorizontal();


            GUILayout.Label("Controls:");
            RKEGUILayout.TextGrid(texts);
          
        }

        void drawToolbars(bool eva)
		{
            if (eva)
            {
                GUILayout.BeginHorizontal();
                drawEvaToolbar();
                GUILayout.EndHorizontal();
            }
            else
            {
                BeginHV(true, false);
                drawJoystickToolbar();
                if (ourstate.smallMode)
                {
                    EndHV(true, false);
                    BeginHV(true, false);
                }
                else
                {
                    GUILayout.Space(10);
                }
                drawKeyboardToolbar();
                EndHV(true, false);
            }
		}

		void BeginHV(bool h, bool v)
		{
			if (h) {
				GUILayout.BeginHorizontal();
			} 
			if (v) {
				GUILayout.BeginVertical();
			}
		}
		void EndHV(bool h, bool v)
		{
			if (h) {
				GUILayout.EndHorizontal();
			} 
			if (v) {
				GUILayout.EndVertical();
			}
		}

//		SelectionGridManager<JoystickPaneMode> jpaneToolbar = new SelectionGridManager<JoystickPaneMode>(
//            new GUIContent[] {
//            new GUIContent(MyResources.wasd_icon), 
//            new GUIContent(MyResources.qwes_icon),
//            new GUIContent("RCS")},
//			new JoystickPaneMode[] {JoystickPaneMode.PITCHYAW, JoystickPaneMode.PITCHROLL, JoystickPaneMode.TRANSLATE}
//		);

		void drawJoystickToolbar()
		{
            SelectionGridManager<JoystickPaneMode> jpaneToolbar = new SelectionGridManager<JoystickPaneMode>(
                new GUIContent[] {
                new GUIContent(MyResources.wasd_icon), 
                new GUIContent(MyResources.qwes_icon),
                new GUIContent(MyResources.rcs_icon),
                new GUIContent(MyResources.rover_icon)},
                new JoystickPaneMode[] {
                JoystickPaneMode.PITCHYAW, JoystickPaneMode.PITCHROLL, JoystickPaneMode.TRANSLATE,
                JoystickPaneMode.ROVER
            }
            );

//			GUILayout.BeginHorizontal(); 
            {
                GUIStyle tStyle = new GUIStyle(GUI.skin.label);
                tStyle.alignment = TextAnchor.MiddleCenter;

                GUILayout.Label(new GUIContent(MyResources.joystick_icon), tStyle);
				JoystickPaneMode prev = jpane.getStickMode();
				JoystickPaneMode next = jpaneToolbar.draw(prev);
				if (prev != next) {
					jpane.setStickMode(next);
                    ourstate.wantResize = true;
				}
			}

//			GUILayout.EndHorizontal();
		}

        void drawEvaToolbar()
        {
            GUIStyle tStyle = new GUIStyle(GUI.skin.label);
            tStyle.alignment = TextAnchor.MiddleCenter;

            GUILayout.Label(new GUIContent(MyResources.joystick_icon), tStyle);

            if (RKEGUILayout.StateButton(GameSettings.EVA_ROTATE_ON_MOVE, new GUIContent(MyResources.rot_on_move_icon),
                null,  Color.green, GUILayout.ExpandWidth(false)))
            {
                GameSettings.EVA_ROTATE_ON_MOVE = !GameSettings.EVA_ROTATE_ON_MOVE;
            }

            {
                GUIStyle tstyle = new GUIStyle(GUI.skin.label);
                tstyle.fontSize = 10;
                GUILayout.Label("(EVA joystick is experimental)", tstyle);
            }
        }

        static bool AnyTrim() {
            var s = FlightInputHandler.state;
            return (s.pitchTrim != 0f)
                || (s.rollTrim != 0f)
                || (s.yawTrim != 0f)
                || (s.wheelSteerTrim != 0f)
                || (s.wheelThrottleTrim != 0f);
        }
		void drawKeyboardToolbar()
		{
//			GUILayout.BeginHorizontal(); 
            {
                GUIStyle tStyle = new GUIStyle(GUI.skin.label);
                tStyle.alignment = TextAnchor.MiddleCenter;

                GUILayout.Label(new GUIContent(MyResources.keyboard_icon), tStyle);

                FlightInputHandler fih = FlightInputHandler.fetch;
                Texture2D preciseTex = fih.precisionMode ? MyResources.precise_icon : MyResources.imprecise_icon;
                bool precise = RKEGUILayout.ToggleButton(fih.precisionMode, new GUIContent(preciseTex), null, XKCDColors.BrightCyan,
                    GUILayout.ExpandWidth(false)
                );
                if (precise != fih.precisionMode)
                {
                    ChangePreciceMode(fih, precise);
                }

                float prev = jpane.getKeyboardLimit();
                float next = GUILayout.HorizontalSlider(prev, 0f, 1f,GUILayout.ExpandWidth(true));
                if (Event.current.type == EventType.Repaint) {
                    kbSliderRect = GUILayoutUtility.GetLastRect();
                }
                if (prev != next)
                {
                    jpane.setKeyboardLimit(next);
                }

                Matrix4x4 orig = GUI.matrix;
                {
                    GUIStyle tipStyle = new GUIStyle(GUI.skin.label);
                    tipStyle.fontSize = 11;
                    tipStyle.alignment = TextAnchor.UpperLeft;
                    tipStyle.wordWrap = false;

                    Rect lr = new Rect(kbSliderRect.x, kbSliderRect.y + kbSliderRect.height, 100, tipStyle.fontSize);
                    GUI.Label(lr, "kb limit " + (next < 1 ? String.Format("{0,3:F0}%", next*100) : "off"), tipStyle);
                }
                GUI.matrix = orig;

			}
//			GUILayout.EndHorizontal();
		}

        void ChangePreciceMode(FlightInputHandler fih, bool precise)
        {
            fih.precisionMode = precise;
            // bit of a hack, but do ourselves what KSP does
            foreach (Renderer g in fih.inputGaugeRenderers) {
                g.material.color = precise ? XKCDColors.BrightCyan : XKCDColors.Orange;
            }
        }

        void drawStatusBar() {
            GUILayout.BeginHorizontal();
            {
                var rcsColor = XKCDColors.BrightYellowGreen;
                if (IsEVA())
                {
                    var kerbal = currentVessel.GetComponent<KerbalEVA>();
                    if (kerbal && RKEGUILayout.MicroButton(kerbal.JetpackDeployed, "RCS", rcsColor))
                    {
                        if (((System.Object)dynToggleJetpack) != null)
                        {
                            dynToggleJetpack.Invoke(kerbal, new object[] {!kerbal.JetpackDeployed});
                        }
                    }
                }
                else
                {
                    if (RKEGUILayout.MicroButton(FlightGlobals.ActiveVessel.ActionGroups[KSPActionGroup.RCS], "RCS", rcsColor))
                    {
                        FlightGlobals.ActiveVessel.ActionGroups.ToggleGroup(KSPActionGroup.RCS);
                        FlightInputHandler.fetch.rcslock = !FlightInputHandler.fetch.rcslock;
                    }

                    if (jpane.IsManualOverride())
                    {
                        RKEGUILayout.MicroButton(true, "SAS", XKCDColors.Butter);
                    }
                    else {
                        if (RKEGUILayout.MicroButton(
                            FlightGlobals.ActiveVessel.ActionGroups[KSPActionGroup.SAS], 
                            "SAS", XKCDColors.VeryPaleBlue))
                        {
                            FlightInputHandler.state.killRot = !FlightInputHandler.state.killRot;
                            FlightGlobals.ActiveVessel.ActionGroups.ToggleGroup(KSPActionGroup.SAS);
                        }
                    }

                    if (RKEGUILayout.MicroButton(AnyTrim(), "TRIM", MyResources.trimColor))
                    {
                        FlightInputHandler.state.ResetTrim();
                        FlightInputHandler.state.wheelThrottleTrim = 0f; // they forgot one
                    }
                }

                GUILayout.FlexibleSpace();
                RKEGUILayout.MicroLabel("RKE Joystick");
                GUILayout.FlexibleSpace();

                if (RKEGUILayout.MicroButton(!ourstate.smallMode, "BIGGER", XKCDColors.Algae))
                {
                    ourstate.smallMode = !ourstate.smallMode;
                    ourstate.wantResize = true;
                    jpane.resetSize();
                }

            }
            GUILayout.EndHorizontal();
		}

		public static Rect constrainToScreen(Rect r)
		{
			r.x = (int)Mathf.Clamp(r.x, 0, Screen.width - r.width);
			r.y = (int)Mathf.Clamp(r.y, 0, Screen.height - r.height);
			return r;
		}
	}

	class SelectionGridManager<T> {
        private GUIContent[] buttons;
		private List<T> values;

        public SelectionGridManager(GUIContent[] text, T[] values) {
			buttons = text;
			this.values = new List<T>(values);
		}

		public T draw(T current) {
			int previ = values.IndexOf(current);
			int nexti = previ;

			for (int i = 0; i < buttons.Length; i++) {
                bool pressed = RKEGUILayout.StateButton(i == previ, new GUIContent(buttons[i]), null, Color.green, 
                    GUILayout.ExpandWidth(false));
				if (pressed) {
					nexti = i;
				}
			}

			return values[nexti];
		}
	}

	class ThrottleControl {
		bool tSetThrottle = false;
		public float tselected = 0;
        Rect sliderRect;

		public void flyit(FlightCtrlState state)
		{
			if (tSetThrottle) {
				FlightInputHandler.state.mainThrottle = tselected;
				tSetThrottle = false;
			} else {
				tselected = state.mainThrottle;
			}
		}

		public  void drawThrottle()
		{
			GUILayout.BeginVertical(); {

                GUIStyle bStyle = new GUIStyle(GUI.skin.button);
                bStyle.fontStyle = FontStyle.Bold;
                bStyle.margin = new RectOffset(0,0,0,0); // squish them up a bit
//                bStyle.fontSize = 12;

				float t = tselected;
                if (GUILayout.Button(GameSettings.THROTTLE_FULL.name, bStyle, GUILayout.ExpandHeight(false))) {
					t = 1f;
				}

                t = GUILayout.VerticalSlider(t, 1, 0);
                if (Event.current.type == EventType.Repaint) {
                    sliderRect = GUILayoutUtility.GetLastRect();
                }

                if (GUILayout.Button(GameSettings.THROTTLE_CUTOFF.name, bStyle, GUILayout.ExpandHeight(false))) {
					t = 0f;
				}
				if (t != tselected) {
					tSetThrottle = true;
					tselected = t;
				}

				
                Matrix4x4 orig = GUI.matrix;
                {
                    GUIStyle tStyle = new GUIStyle(GUI.skin.label);
                    tStyle.fontSize = 12;
                    tStyle.alignment = TextAnchor.UpperCenter;
                    tStyle.wordWrap = false;

                    Rect lr = new Rect(0, 0, 100, tStyle.fontSize);
                    Vector2 p = new Vector2(
                        sliderRect.x - tStyle.fontSize, 
                        sliderRect.y + (sliderRect.height + lr.width)/2);
                    GUIUtility.RotateAroundPivot(-90, p);
                    GUI.matrix *= JoystickPane.Translate4x4((Vector3) p);
                    GUI.Label(lr, String.Format("Throttle: {0,3:F0}%", tselected * 100), tStyle);
                }
                GUI.matrix = orig;
			}
			GUILayout.EndVertical();
		}
	}


	#if DEBUG
	//This will kick us into the save called default and set the first vessel active
	[KSPAddon(KSPAddon.Startup.MainMenu, false)]
	public class Debug_AutoLoadPersistentSaveOnStartup : MonoBehaviour
	{
		//use this variable for first run to avoid the issue with when this is true and multiple addons use it
		public static bool first = true;
		public void Start()
		{
			//only do it on the first entry to the menu
			if (first)
			{
				Debug.Log("RKE_Joystick debug mode");
				first = false;
				HighLogic.SaveFolder = "default";
				Game game = GamePersistence.LoadGame("persistent", HighLogic.SaveFolder, true, false);
				
				if (game != null && game.flightState != null && game.compatible)
				{
					Int32 FirstVessel;
					Boolean blnFoundVessel=false;
					for (FirstVessel = 0; FirstVessel < game.flightState.protoVessels.Count; FirstVessel++)
					{
						//This logic finds the first non-asteroid vessel
						if (game.flightState.protoVessels[FirstVessel].vesselType != VesselType.SpaceObject &&
						    game.flightState.protoVessels[FirstVessel].vesselType != VesselType.Unknown)
						{
							////////////////////////////////////////////////////
							//PUT ANY OTHER LOGIC YOU WANT IN HERE//
							////////////////////////////////////////////////////
							blnFoundVessel = true;
							break;
						}
					}
					if (!blnFoundVessel)
						FirstVessel = 0;
					FlightDriver.StartAndFocusVessel(game, FirstVessel);
				}
				
                CheatOptions.InfiniteFuel = true;
                CheatOptions.InfiniteRCS = true;
                CheatOptions.InfiniteEVAFuel = true;
			}
		}
	}
	#endif
}

