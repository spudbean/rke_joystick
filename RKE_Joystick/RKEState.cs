﻿using System;
using UnityEngine;
using System.IO;

namespace RKE_Joystick
{
    public class RKEState : MonoBehaviour
    {
        public Vector2 windowPosition = new Vector2(0, 0);
        public bool windowVisible = true;
        public bool smallMode = true;
        public float minTransparency = 0;
        public float fullscreenDeadRadius = 30f;
        public float fullscreenSize = 3f / 4f;
        public bool fullscreen = false;
        public string bindingStr = "";
        public bool wantResize = false;

        private KeyCode bindingKey;
        private EventModifiers bindingModifiers;

        public RKEState()
        {
        }

        public float deadRadius {
            get { 
                return fullscreen ? fullscreenDeadRadius : 5f;
            }
        }

        public void setBinding(string str)
        {
            this.bindingStr = str;
            CustomKeyGUI.parse(str, ref bindingKey, ref bindingModifiers);
        }

        public bool getBindingKeyDown() {
            if (Input.GetKeyDown(bindingKey))
            {
                if (bindingModifiers.IsFlagSet(EventModifiers.Shift) && !(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
                {
                    return false;
                }
                if (bindingModifiers.IsFlagSet(EventModifiers.Control) && !(Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
                {
                    return false;
                }
                if (bindingModifiers.IsFlagSet(EventModifiers.Alt) && !(Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)))
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public void Awake()
        {
            try {
                loadConfig();
            } catch (Exception ex) {
                JoystickMod.LogEventError("Awake", ex);
            }
        }

        public void OnDestroy() {
            try {
                saveConfig();
            } catch (Exception ex) {
                JoystickMod.LogEventError("OnDestroy", ex);
            }

        }

        void loadConfig()
        {
            if (File.Exists(MyResources.SETTINGS_FILE_URL)) {
                ConfigNode node = ConfigNode.Load(MyResources.SETTINGS_FILE_URL);
                tryParse(node, "small", ref smallMode);
                tryParse(node, "visible", ref windowVisible);
                tryParse(node, "fullscreen", ref fullscreen);
                tryParse(node, "pos", ref windowPosition);
                tryParse(node, "minTransparency", ref minTransparency);
                tryParse(node, "fullscreenDeadRadius", ref fullscreenDeadRadius);
                tryParse(node, "fullscreenSize", ref fullscreenSize);
                tryParse(node, "binding", ref bindingStr);
            } else {
                // defaults
                smallMode = true;
                windowPosition.x = 200;
                windowPosition.y = 200;
                windowVisible = true;
                fullscreen = false;
                bindingStr = "Alt+Y";
            }
            setBinding(bindingStr);
            wantResize = true;
        }

        void saveConfig()
        {
            ConfigNode node = new ConfigNode();
            node.AddValue("small", smallMode);
            node.AddValue("visible", windowVisible);
            node.AddValue("fullscreen", fullscreen);
            node.AddValue("pos", settingsString(windowPosition));
            node.AddValue("minTransparency", minTransparency);
            node.AddValue("fullscreenDeadRadius", fullscreenDeadRadius);
            node.AddValue("fullscreenSize", fullscreenSize);
            node.AddValue("binding", bindingStr);
            node.Save(MyResources.SETTINGS_FILE_URL);
        }

        static void tryParse(ConfigNode node, string name, ref bool value)
        {
            if (node.HasValue(name)) {
                String s = node.GetValue(name);
                bool.TryParse(s, out value);
            }
        }
        static void tryParse(ConfigNode node, string name, ref float value)
        {
            if (node.HasValue(name)) {
                String s = node.GetValue(name);
                float.TryParse(s, out value);
            }
        }
        static void tryParse(ConfigNode node, string name, ref string value)
        {
            if (node.HasValue(name)) {
                value = node.GetValue(name);
            }
        }

        static string settingsString(Vector2 position)
        {
            return position.x + "," + position.y;
        }

        static bool tryParse(ConfigNode node, string name, ref Vector2 value)
        {
            if (node.HasValue(name)) {
                String s = node.GetValue(name);
                string[] a = s.Split(new char[] {','});
                if (a.Length == 2) {
                    float x, y;
                    if (float.TryParse(a[0], out x) && float.TryParse(a[1], out y)) {
                        value = new Vector2(x, y);
                        return true;
                    }
                }
            }
            return false;
        }


    }
}

