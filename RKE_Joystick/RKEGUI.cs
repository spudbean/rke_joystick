﻿using System;
using UnityEngine;

namespace RKE_Joystick
{
    public static class RKEGUILayout
    {
        public static void TextGrid(string[][] texts)
        {
            GUIStyle tstyle = new GUIStyle(GUI.skin.label);
            tstyle.wordWrap = false;
            tstyle.alignment = TextAnchor.UpperLeft;
            tstyle.margin.bottom = 0;
            tstyle.margin.right = 10;
            tstyle.padding.bottom = 0;
            tstyle.contentOffset = new Vector2(0, 0);
            float maxwidth = 0;
            foreach (var a in texts)
            {
                maxwidth = Mathf.Max(tstyle.CalcSize(new GUIContent(a[0])).x, maxwidth);
            }
            GUILayout.BeginVertical();
            foreach (var a in texts)
            {
                GUILayout.BeginHorizontal();
                for (int i = 0; i < a.Length; i++) {
                    if (i == 0)
                    {
                        GUILayout.Label(a[i], tstyle, GUILayout.MinWidth(maxwidth));
                    }
                    else
                    {
                        GUILayout.Label(a[i], tstyle);
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }

        public static bool StateButton(bool toggle, GUIContent content, Color? inactiveColor, Color activeColor,
            params GUILayoutOption[] options)
        {
            return StateButton(toggle, content, inactiveColor, activeColor, GUI.skin.button, options);
        }
        public static bool StateButton(bool toggle, GUIContent content, Color? inactiveColor, Color activeColor,
            GUIStyle style,
            params GUILayoutOption[] options)
        {
            Color oldBgColor = GUI.backgroundColor;
            if (!inactiveColor.HasValue)
            {
                inactiveColor = GUI.color;
            }
            GUI.backgroundColor = toggle ? activeColor : inactiveColor.Value;
            bool pressed = GUILayout.Button(content, style, options);
            GUI.backgroundColor = oldBgColor;
            return pressed;
        }
        public static bool ToggleButton(bool current, GUIContent content, Color? inactiveColor, Color activeColor,
            params GUILayoutOption[] options) 
        {
            bool toggled = StateButton(current, content, inactiveColor, activeColor, options);
            return toggled ? !current : current;
        }

        public static bool MicroButton(bool current, string txt, Color selectedColor, Color? unselectedColor = null)
        {
            Color origColor = GUI.color;
            Color origBg = GUI.backgroundColor;

            GUIStyle tstyle = MicroTextStyle();
            tstyle.alignment = TextAnchor.LowerCenter;
//            tstyle.fixedWidth = txt.Length * tstyle.fontSize;
            if (current)
            {
                GUI.color = selectedColor;
                tstyle.normal.background = Drawing.LoadTexture();
                tstyle.normal.textColor = Color.black;
                tstyle.fontStyle = FontStyle.Bold;
            }
            else
            {
                tstyle.normal.textColor = unselectedColor.HasValue ? unselectedColor.Value : Color.gray;
            }

            bool pressed = GUILayout.Button(txt, tstyle,
                GUILayout.ExpandWidth(false));

            GUI.color = origColor;
            GUI.backgroundColor = origBg;
            return pressed;

        }
        public static void MicroLabel(string txt)
        {
            Color origColor = GUI.color;
            Color origBg = GUI.backgroundColor;

            GUIStyle tstyle = MicroTextStyle();
            tstyle.alignment = TextAnchor.LowerCenter;
//            tstyle.normal.textColor = Color.gray;
            GUILayout.Label(txt, tstyle,
                GUILayout.ExpandWidth(false));

            GUI.color = origColor;
            GUI.backgroundColor = origBg;
        }

        static GUIStyle MicroTextStyle()
        {
            GUIStyle tstyle = new GUIStyle(GUI.skin.label);
            tstyle.padding = new RectOffset(0,0,0,0);
            tstyle.margin = new RectOffset(1,1,1,0);
            tstyle.contentOffset = new Vector2(0,0);
            tstyle.fontSize = 9;
            return tstyle;
        }
    }
}

