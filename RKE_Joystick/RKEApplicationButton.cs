﻿using System;
using UnityEngine;

namespace RKE_Joystick
{
    public class RKEApplicationButton : MonoBehaviour
    {
        private ApplicationLauncherButton appButton = null;
        private RKEApplicationMenu menu;
        private RKEState state;

        public RKEApplicationButton()
        {
        }

        public void Awake()
        {
            try {
                GameEvents.onGUIApplicationLauncherReady.Add(OnGUIAppLauncherReady);
                state = gameObject.GetComponent<RKEState>();

                if (ApplicationLauncher.Ready) {
                    OnGUIAppLauncherReady();
                }
            } catch (Exception ex) {
                JoystickMod.LogEventError("Awake", ex);
            }
        }

        public void OnDestroy() {
            try {
                GameEvents.onGUIApplicationLauncherReady.Remove(OnGUIAppLauncherReady);
                if (appButton != null) {
                    ApplicationLauncher.Instance.RemoveModApplication(appButton);
                }
            } catch (Exception ex) {
                JoystickMod.LogEventError("OnDestroy", ex);
            }

        }

        void OnGUIAppLauncherReady() {
            try {
                if (ApplicationLauncher.Ready && appButton == null) {
                    var modes = ApplicationLauncher.AppScenes.FLIGHT
                        | ApplicationLauncher.AppScenes.MAPVIEW;

                    appButton = ApplicationLauncher.Instance.AddModApplication(
                        onAppLaunchToggleOnOff, 
                        onAppLaunchToggleOnOff, 
                        onHover, onHoverOut,
                        null, null, 
                        modes,
                        MyResources.appButton);

                    menu = this.appButton.gameObject.AddComponent<RKEApplicationMenu>();
                    menu.init(state);
                    menu.transform.parent = this.appButton.transform;
                }

            } catch (Exception ex) {
                JoystickMod.LogEventError("OnGUIAppLauncherReady", ex);
            }
        }

        void Update() {
            if (appButton)
            {
                if (state.windowVisible)
                {
                    appButton.SetTrue(false);
                }
                else
                {
                    appButton.SetFalse(false);
                }
            }
        }
        void onAppLaunchToggleOnOff() {
            state.windowVisible = appButton.State == RUIToggleButton.ButtonState.TRUE;
        }


        void onHover()
        {
            menu.hovering = true;
        }

        void onHoverOut()
        {
            menu.hovering = false;
        }
    }
}

