RKE Joystick
version @RKEVERSION@

Forum Thread:  http://forum.kerbalspaceprogram.com/threads/92586
Source code:   https://bitbucket.org/spudbean/rke_joystick
License:       Apache 2.0

Installation
============

1. Unzip
2. Copy RKE_Joystick into <KSP_OS>\GameData
   (Delete existing RKE_Joystick directory if it exists.)


Copyright
=========

Copyright 2014 Matt Quail

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
