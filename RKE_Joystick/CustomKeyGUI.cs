﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RKE_Joystick
{
    public class CustomKeyGUI : MonoBehaviour
    {
        static IDictionary<KeyCode, string> KEYS = new Dictionary<KeyCode, string>();

        static CustomKeyGUI() {
            KEYS.Add(KeyCode.Backspace, "backspace");
            KEYS.Add(KeyCode.Tab, "tab");
            KEYS.Add(KeyCode.Return, "return");
            KEYS.Add(KeyCode.Space, "space");
            KEYS.Add(KeyCode.Exclaim, "!");
            KEYS.Add(KeyCode.DoubleQuote, "\"");
            KEYS.Add(KeyCode.Hash, "#");
            KEYS.Add(KeyCode.Dollar, "$");
            KEYS.Add(KeyCode.Ampersand, "&");
            KEYS.Add(KeyCode.Quote, "'");
            KEYS.Add(KeyCode.LeftParen, "(");
            KEYS.Add(KeyCode.RightParen, ")");
            KEYS.Add(KeyCode.Asterisk, "*");
            KEYS.Add(KeyCode.Plus, "+");
            KEYS.Add(KeyCode.Comma, ",");
            KEYS.Add(KeyCode.Minus, "-");
            KEYS.Add(KeyCode.Period, ".");
            KEYS.Add(KeyCode.Slash, "/");
            KEYS.Add(KeyCode.Alpha0, "0");
            KEYS.Add(KeyCode.Alpha1, "1");
            KEYS.Add(KeyCode.Alpha2, "2");
            KEYS.Add(KeyCode.Alpha3, "3");
            KEYS.Add(KeyCode.Alpha4, "4");
            KEYS.Add(KeyCode.Alpha5, "5");
            KEYS.Add(KeyCode.Alpha6, "6");
            KEYS.Add(KeyCode.Alpha7, "7");
            KEYS.Add(KeyCode.Alpha8, "8");
            KEYS.Add(KeyCode.Alpha9, "9");
            KEYS.Add(KeyCode.Colon, ":");
            KEYS.Add(KeyCode.Semicolon, ";");
            KEYS.Add(KeyCode.Less, "<");
            KEYS.Add(KeyCode.Equals, "=");
            KEYS.Add(KeyCode.Greater, ">");
            KEYS.Add(KeyCode.Question, "?");
            KEYS.Add(KeyCode.At, "@");
            KEYS.Add(KeyCode.LeftBracket, "[");
            KEYS.Add(KeyCode.Backslash, "\\");
            KEYS.Add(KeyCode.RightBracket, "]");
            KEYS.Add(KeyCode.Caret, "^");
            KEYS.Add(KeyCode.Underscore, "_");
            KEYS.Add(KeyCode.BackQuote, "`");
            KEYS.Add(KeyCode.A, "A");
            KEYS.Add(KeyCode.B, "B");
            KEYS.Add(KeyCode.C, "C");
            KEYS.Add(KeyCode.D, "D");
            KEYS.Add(KeyCode.E, "E");
            KEYS.Add(KeyCode.F, "F");
            KEYS.Add(KeyCode.G, "G");
            KEYS.Add(KeyCode.H, "H");
            KEYS.Add(KeyCode.I, "I");
            KEYS.Add(KeyCode.J, "J");
            KEYS.Add(KeyCode.K, "K");
            KEYS.Add(KeyCode.L, "L");
            KEYS.Add(KeyCode.M, "M");
            KEYS.Add(KeyCode.N, "N");
            KEYS.Add(KeyCode.O, "O");
            KEYS.Add(KeyCode.P, "P");
            KEYS.Add(KeyCode.Q, "Q");
            KEYS.Add(KeyCode.R, "R");
            KEYS.Add(KeyCode.S, "S");
            KEYS.Add(KeyCode.T, "T");
            KEYS.Add(KeyCode.U, "U");
            KEYS.Add(KeyCode.V, "V");
            KEYS.Add(KeyCode.W, "W");
            KEYS.Add(KeyCode.X, "X");
            KEYS.Add(KeyCode.Y, "Y");
            KEYS.Add(KeyCode.Z, "Z");
            KEYS.Add(KeyCode.Delete, "delete");
            KEYS.Add(KeyCode.Keypad0, "[0]");
            KEYS.Add(KeyCode.Keypad1, "[1]");
            KEYS.Add(KeyCode.Keypad2, "[2]");
            KEYS.Add(KeyCode.Keypad3, "[3]");
            KEYS.Add(KeyCode.Keypad4, "[4]");
            KEYS.Add(KeyCode.Keypad5, "[5]");
            KEYS.Add(KeyCode.Keypad6, "[6]");
            KEYS.Add(KeyCode.Keypad7, "[7]");
            KEYS.Add(KeyCode.Keypad8, "[8]");
            KEYS.Add(KeyCode.Keypad9, "[9]");
            KEYS.Add(KeyCode.KeypadPeriod, "[.]");
            KEYS.Add(KeyCode.KeypadDivide, "[/]");
            KEYS.Add(KeyCode.KeypadMultiply, "[*]");
            KEYS.Add(KeyCode.KeypadMinus, "[-]");
            KEYS.Add(KeyCode.KeypadPlus, "[+]");
            KEYS.Add(KeyCode.KeypadEnter, "[enter]");
            KEYS.Add(KeyCode.KeypadEquals, "[=]");
            KEYS.Add(KeyCode.UpArrow, "up");
            KEYS.Add(KeyCode.DownArrow, "down");
            KEYS.Add(KeyCode.RightArrow, "right");
            KEYS.Add(KeyCode.LeftArrow, "left");
            KEYS.Add(KeyCode.Insert, "insert");
            KEYS.Add(KeyCode.Home, "home");
            KEYS.Add(KeyCode.End, "end");
            KEYS.Add(KeyCode.PageUp, "pageup");
            KEYS.Add(KeyCode.PageDown, "pagedown");
            KEYS.Add(KeyCode.F1, "F1");
            KEYS.Add(KeyCode.F2, "F2");
            KEYS.Add(KeyCode.F3, "F3");
            KEYS.Add(KeyCode.F4, "F4");
            KEYS.Add(KeyCode.F5, "F5");
            KEYS.Add(KeyCode.F6, "F6");
            KEYS.Add(KeyCode.F7, "F7");
            KEYS.Add(KeyCode.F8, "F8");
            KEYS.Add(KeyCode.F9, "F9");
            KEYS.Add(KeyCode.F10, "F10");
            KEYS.Add(KeyCode.F11, "F11");
            KEYS.Add(KeyCode.F12, "F12");
            KEYS.Add(KeyCode.F13, "F13");
            KEYS.Add(KeyCode.F14, "F14");
            KEYS.Add(KeyCode.F15, "F15");
        }

        public static void parse(string str, ref KeyCode bindingKey, ref EventModifiers bindingModifiers)
        {
            bindingKey = KeyCode.None;
            bindingModifiers = 0;
            if (str != null || str == "")
            {
                while (true)
                {
                    if (str.StartsWith("Alt+"))
                    {
                        bindingModifiers |= EventModifiers.Alt;
                        str = str.Substring("Alt+".Length);
                    }
                    else if (str.StartsWith("Ctrl+"))
                    {
                        bindingModifiers |= EventModifiers.Control;
                        str = str.Substring("Ctrl+".Length);
                    }
                    else if (str.StartsWith("Shift+"))
                    {
                        bindingModifiers |= EventModifiers.Shift;
                        str = str.Substring("Shift+".Length);
                    }
                    else
                    {
                        break;
                    }
                }
                foreach (KeyValuePair<KeyCode, string> entry in KEYS)
                {
                    if (entry.Value == str)
                    {
                        bindingKey = entry.Key;
                        break;
                    }
                }
            }
        }

        private bool _show = false;
        private Rect windowPosition;
        private RKEState state;
        private string existingBinding;
        private string newBinding;

        public void init(RKEState state)
        {
            this.state = state;
        }

        public CustomKeyGUI()
        {
        }

        public void show(string existingBinding, Vector2 pos)
        {
            _show = true;
            if (existingBinding == null || existingBinding == "")
            {
                existingBinding = "(none)";
            }
            this.existingBinding = existingBinding;
            this.newBinding = null;
            windowPosition.min = pos;
        }

        private void OnGUI()
        {
            if (!_show)
            {
                return;
            }
            GUI.skin = HighLogic.Skin;
            windowPosition = JoystickMod.constrainToScreen(windowPosition);
            windowPosition = GUILayout.Window(this.GetInstanceID(), windowPosition, this.onWindow, "RKE Joystick Keybinding");
            GUI.skin = null;
        }
        void onWindow(int id)
        {
            try {
                GUILayout.BeginVertical(GUILayout.MinWidth(200)); {
                    KeyCode kc = KeyCode.None;
                    string key = null;
                    foreach(KeyValuePair<KeyCode, string> entry in KEYS) {
                        if (Input.GetKey(entry.Key)) {
                            kc = entry.Key;

                            string mod = "";
                            if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
                                mod += "Alt+";
                            }
                            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) {
                                mod += "Ctrl+";
                            }
                            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                                mod += "Shift+";
                            }

                            key = mod + entry.Value;
                            break;
                        }
                    }
                    if (key != null) {
                        newBinding = key;
                    }

                    GUILayout.Label("Existing binding: " + existingBinding);
                    if (newBinding != null) {
                        GUILayout.Label("New binding: " + newBinding);
                    }
                    else {
                        GUILayout.Label("Enter new binding now...");
                    }

                    GUILayout.BeginHorizontal(); {
                        if (newBinding != null) {
                            if (GUILayout.Button("Accept")) {
                                _show = false;
                                state.setBinding(newBinding);
                            }
                        }
                        if (GUILayout.Button("Clear")) {
                            _show = false;
                            state.setBinding(null);
                        }
                        if (GUILayout.Button("Cancel")) {
                            _show = false;
                        }
                    } GUILayout.EndHorizontal();



                }
                GUILayout.EndVertical();
            }
            catch (Exception e) {
                JoystickMod.LogEventError("onWindow", e);
            }
        }

    }
}

