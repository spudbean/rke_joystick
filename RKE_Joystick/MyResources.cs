using System;
using UnityEngine;

namespace RKE_Joystick
{
    public class TextureCursor {
        public Texture2D texture;
        public Vector2 offset;
        public TextureCursor(Texture2D texture, Vector2 offset)
        {
            this.texture = texture;
            this.offset = offset;
        }
        
    }
	public static class MyResources
	{
        private static bool loaded = false;
		public const string rootname = "RKE_Joystick";
		public const string SETTINGS_FILE_URL = "GameData/" + rootname + ".cfg";

        public static Color trimColor = new Color(1.0f, 0.3f, 0.3f);

        public static Texture2D keyboard_icon;
        public static Texture2D joystick_icon;
        public static Texture2D precise_icon;
        public static Texture2D imprecise_icon;
        public static Texture2D qwes_icon;
        public static Texture2D wasd_icon;
        public static Texture2D rcs_icon;
        public static Texture2D rover_icon;
        public static Texture2D rot_on_move_icon;
//        public static Texture2D minmax_icon;

        public static Texture2D appButton;

        public static TextureCursor cursor_1;
        public static TextureCursor cursor_2;
        public static TextureCursor cursor_3;
        public static TextureCursor cursor_fwd;
        public static TextureCursor cursor_rover_1;
        public static TextureCursor cursor_rover_2;
        public static TextureCursor cursor_eva_1;
        public static TextureCursor cursor_eva_2;

        static Texture2D LoadTexture(string name)
        {
            var result = GameDatabase.Instance.GetTexture(rootname + name, false);
            if (result == null)
            {
                Debug.Log("RKE: could not load image " + name);
            }
            return result;
        }

		public static void loadResources() {
            if (loaded)
            {
                return;
            }
            loaded = true;
//			Debug.Log("Loading textures " + rootname);

            keyboard_icon = LoadTexture("/images/keyboard_icon");
            joystick_icon = LoadTexture("/images/joystick_icon");
            precise_icon = LoadTexture("/images/precise");
            imprecise_icon = LoadTexture("/images/imprecise");
            qwes_icon = LoadTexture("/images/qwes_icon");
            wasd_icon = LoadTexture("/images/wasd_icon");
            rcs_icon = LoadTexture("/images/rcs_icon");
            rover_icon = LoadTexture("/images/rover_icon");
            rot_on_move_icon = LoadTexture("/images/rot_on_move");
//            minmax_icon = LoadTexture("/images/minmax");

			appButton = LoadTexture("/images/icon");

            Texture2D t;
            t = LoadTexture("/images/plane_cursor_1");
            cursor_1 = new TextureCursor(t, new Vector2(t.width / 2f, 22f));

            t = LoadTexture("/images/plane_cursor_2");
            cursor_2 = new TextureCursor(t, new Vector2(t.width / 2f, 24.5f));

            t = LoadTexture("/images/plane_cursor_3");
            cursor_3 = new TextureCursor(t, new Vector2(4f, t.height/2f));

            t = LoadTexture("/images/plane_cursor_fwd");
            cursor_fwd = new TextureCursor(t, new Vector2(t.width / 2f, 20f));

            t = LoadTexture("/images/rover_cursor_1");
            cursor_rover_1 = new TextureCursor(t, new Vector2(t.width / 2f, t.height/2));

            t = LoadTexture("/images/rover_cursor_2");
            cursor_rover_2 = new TextureCursor(t, new Vector2(t.width / 2f, t.height/2));

            t = LoadTexture("/images/eva_cursor_1");
            cursor_eva_1 = new TextureCursor(t, new Vector2(t.width / 2f, t.height/2));

            t = LoadTexture("/images/eva_cursor_2");
            cursor_eva_2 = new TextureCursor(t, new Vector2(t.width / 2f, t.height/2));


		}
	}
}

